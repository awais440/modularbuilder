import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./components/navHeader/header";
import NavLinks from "./components/navHeader/navLinks";
import { Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
      <NavLinks />
    </div>
  );
}

export default App;
