import React, { Component } from "react";

class Test extends Component {
  state = {
    range: 0,
  };
  handleChange = (e) => {
    this.setState({ range: e.target.value });
    console.log(this.state.range);
  };
  render() {
    return (
      <div>
        <div className="flex">
          <p>-3A</p>
          <input
            type="range"
            min="3"
            max="215"
            value={this.state.range}
            onChange={this.handleChange}
            className="range"
          />
          <p>+215A</p>
        </div>
      </div>
    );
  }
}

export default Test;
