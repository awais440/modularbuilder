import React, { Component } from "react";

class WireSize extends Component {
  state = {
    rangeWire: 0,
  };
  handleWireValue = (e) => {
    this.setState({ rangeWire: e.target.value });
    console.log(this.state.rangeWire);
  };
  handleNegativeRange = () => {
    const minValue = 0;

    if (this.state.rangeWire === minValue) {
      return null;
    } else {
      let decrement = parseInt(this.state.rangeWire) - 5;
      this.setState({ rangeWire: decrement });
    }
  };
  handlePositiveRange = () => {
    const maxValue = 35;

    if (this.state.rangeWire === maxValue) {
      return null;
    } else {
      let increment = parseInt(this.state.rangeWire) + 5;
      this.setState({ rangeWire: increment });
    }
  };
  render() {
    return (
      <div className="basic-data">
        <div className="text">
          <h2>Max. Wire Size</h2>
          <p>
            Wire cross-section in mm² connected to the power contacts. Please
            select the wire size of your biggest cross-section.
          </p>
        </div>

        <div className="range-slider">
          <button className="range-move-btn" onClick={this.handleNegativeRange}>
            <span>-</span> <br />
            13mm²
          </button>
          <div
            className="range"
            style={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
            }}
          >
            <p style={{ textAlign: "center", marginBottom: "20px" }}>
              {this.state.rangeWire}mm²
            </p>
            <input
              type="range"
              name="rangeWire"
              min="13"
              max="35"
              value={this.state.rangeWire}
              onChange={this.handleWireValue}
              className="range-input"
            />
          </div>
          <button className="range-move-btn" onClick={this.handlePositiveRange}>
            <span>+</span>
            <br /> 35mm²
          </button>
        </div>
      </div>
    );
  }
}

export default WireSize;
