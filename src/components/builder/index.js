import React from "react";
import { Switch, Route } from "react-router-dom";
import Start from "./start/start";
import Basics from "./basics/basics";
import Current from "./basics/current";
import WireSize from "./basics/wire";
const Builder = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={Start} />

      </Switch>
    </div>
  );
};

export default Builder;
