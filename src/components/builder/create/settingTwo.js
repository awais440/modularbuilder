import React, { Component } from "react";

class SettingTwo extends Component {
  state = {
    rangeWire: 0,
    currentValue: "12",
  };
  handleVoltageValue = (e) => {
    this.setState({ currentValue: e.target.value });
    console.log(this.state.currentValue);
  };
  handleWireValue = (e) => {
    this.setState({ rangeWire: e.target.value });
    console.log(this.state.rangeWire);
  };
  handleNegativeRange = () => {
    const minValue = 0;

    if (this.state.rangeWire === minValue) {
      return null;
    } else {
      let decrement = parseInt(this.state.rangeWire) - 5;
      this.setState({ rangeWire: decrement });
    }
  };
  handlePositiveRange = () => {
    const maxValue = 35;

    if (this.state.rangeWire === maxValue) {
      return null;
    } else {
      let increment = parseInt(this.state.rangeWire) + 5;
      this.setState({ rangeWire: increment });
    }
  };
  render() {
    return (
      <div className="settings-second">
        <div className="range-slider">
          <button className="range-move-btn" onClick={this.handleNegativeRange}>
            <span>-</span> <br />
            13mm²
          </button>
          <div
            className="range"
            style={{
              display: "flex",
              flexDirection: "column",
              width: "100%",
            }}
          >
            <p style={{ textAlign: "center", marginBottom: "20px" }}>
              {this.state.rangeWire}mm²
            </p>
            <input
              type="range"
              name="rangeWire"
              min="13"
              max="35"
              value={this.state.rangeWire}
              onChange={this.handleWireValue}
              className="range-input"
            />
          </div>
          <button className="range-move-btn" onClick={this.handlePositiveRange}>
            <span>+</span>
            <br /> 35mm²
          </button>
        </div>
        <div className="create-checkbox">
          <label className="checkbox">
            <input
              type="checkbox"
              value="48"
              checked={this.state.currentValue === "48"}
              onChange={this.handleVoltageValue}
              className="checkbox"
            />
            <span className="checkbox-custom" />
            <label>12V</label>
          </label>
          <label className="checkbox">
            <input
              type="checkbox"
              value="12"
              checked={this.state.currentValue === "12"}
              onChange={this.handleVoltageValue}
              className="checkbox"
            />
            <span className="checkbox-custom" />
            <label>48V</label>
          </label>
        </div>
        <div className="create-checkbox">
          <label className="checkbox">
            <input
              type="checkbox"
              value="48"
              checked={this.state.currentValue === "48"}
              onChange={this.handleVoltageValue}
              className="checkbox"
            />
            <span className="checkbox-custom" />
            <label>12V</label>
          </label>
          <label className="checkbox">
            <input
              type="checkbox"
              value="12"
              checked={this.state.currentValue === "12"}
              onChange={this.handleVoltageValue}
              className="checkbox"
            />
            <span className="checkbox-custom" />
            <label>48V</label>
          </label>
        </div>
        <button
          className="button"
          style={{ float: "right", margin: "0 15px 5px 0" }}
        >
          Apply
        </button>
      </div>
    );
  }
}
export default SettingTwo;
