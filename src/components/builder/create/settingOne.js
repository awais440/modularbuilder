import React, { Component } from "react";
import card_1 from "../../../assets/img/card-image-1.png";
import card_2 from "../../../assets/img/card-image-2.png";
import card_3 from "../../../assets/img/card-image-3.png";
import card_4 from "../../../assets/img/card-image-4.png";
import SimpleBarReact from "simplebar-react";
import "simplebar/src/simplebar.css";

class SettingOne extends Component {
  render() {
    return (
      <SimpleBarReact style={{ maxHeight: 330 }}>
        <div className="cards">
            <div className="image-card">
              <img src={card_1} alt="single-card" />
              <h5>Name</h5>
            </div>
            <div className="image-card">
              <img src={card_2} alt="single-card" />
              <h5>Name</h5>
            </div>
            <div className="image-card">
              <img src={card_3} alt="single-card" />
              <h5>Name</h5>
            </div>
            <div className="image-card">
              <img src={card_4} alt="single-card" />
              <h5>Name</h5>
            </div>
          </div>
      </SimpleBarReact>
    );
  }
}
export default SettingOne;
