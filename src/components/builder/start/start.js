import React, { Component } from "react";
import "./start.css";
class Start extends Component {
  state = {
    name: "",
    btnActive: false,
    dropDownValue: "",
  };
  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
    console.log(this.state.name);
    if (this.state.name) {
      this.setState({ btnActive: true });
    }
  };

  render() {
    const { name } = this.state;
    return (
      <div className="container">
        <div className="start">
          <h2>Welcome to the all new Modular Builder</h2>
          <p>
            The TE Modular Builder gives you the opportunity to create and view
            entirely new hybrid connectors.<br/> Discover the full potential of
            Modular Builder and create your connector now.
          </p>
          <h5>
            At first, please select the functional area from the drop down list
            below.
          </h5>
          <select
            className="dropdown"
            value={this.state.dropDownValue}
            onChange={(e) => {
              this.setState({ dropDownValue: e.target.value });
            }}
          >
            {console.log(this.state.dropDownValue)}
            <option>Functional Area</option>
            <option>Modular Builder</option>
            <option>Dropdown Menu</option>
          </select>
          <h4>Name Your Component:</h4>
          <input
            type="text"
            name="name"
            value={name}
            onChange={this.handleChange}
            placeholder="Component Name"
          />
          {this.state.btnActive ? (
            <button
              className="button"
              onClick={() => {
                window.location.href = "/basic";
              }}
            >
              Continue
            </button>
          ) : (
            <button disabled={true} className="button button-disabled">
              Continue
            </button>
          )}
        </div>
      </div>
    );
  }
}

export default Start;
